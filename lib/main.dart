import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutterreduxstatepractice/app_state.dart';
import 'package:flutterreduxstatepractice/main_view_model.dart';
import 'package:redux/redux.dart';

import 'app_state_reducer.dart';
import 'application.dart';

void main() => runApp(Application());
