import 'package:flutter/cupertino.dart';
import 'package:flutterreduxstatepractice/actions.dart';
import 'package:flutterreduxstatepractice/app_state.dart';
import 'package:redux/redux.dart';

class AppStateSelector{
  static void Function() getChangeColorOnTheRedFunction(Store<AppState> store){
    return ()=> store.dispatch(ChangeColorOnTheRed);
  }

  static void Function() getChangeColorOnTheYellowFunction(Store<AppState> store){
    return ()=> store.dispatch(ChangeColorOnTheYellow);
  }

  static void Function() getChangeColorOnTheGreenFunction(Store<AppState> store){
    return ()=> store.dispatch(ChangeColorOnTheGreen);
  }

  static Color getCurrentColor(Store<AppState> store){
    return store.state.colors;
  }
}