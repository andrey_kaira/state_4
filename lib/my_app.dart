
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'app_state.dart';
import 'app_state_reducer.dart';
import 'main_view_model.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: Store<AppState>(
        appStateReducer,
        initialState: AppState.initState(),
      ),
      child: StoreConnector<AppState, MainViewModel>(
        converter: (store) => MainViewModel.initState(store),
        builder: (context, colors) {
          return Scaffold(
            appBar: AppBar(
              title: Text('State Management Practice #4'),
            ),
            drawer: Drawer(
              child: Column(
                children: <Widget>[
                  AppBar(
                    automaticallyImplyLeading: false,
                  ),
                  InkWell(
                    child: Container(
                      color: Colors.red,
                      height: 100.0,
                      margin: EdgeInsets.all(15.0),
                    ),
                    onTap: colors.changeColorOnTheRed,
                  ),
                  InkWell(
                    child: Container(
                      color: Colors.yellow,
                      height: 100.0,
                      margin: EdgeInsets.all(15.0),
                    ),
                    onTap: colors.changeColorOnTheYellow,
                  ),
                  InkWell(
                    child: Container(
                      color: Colors.green,
                      height: 100.0,
                      margin: EdgeInsets.all(15.0),
                    ),
                    onTap: colors.changeColorOnTheGreen,
                  ),
                ],
              ),
            ),
            body: Container(
              color: colors.colors,
            ),
          );
        },
      ),
    );
  }
}
