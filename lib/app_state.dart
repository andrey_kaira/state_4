import 'package:flutter/material.dart';

class AppState {
  final Color colors;

  AppState(this.colors);

  factory AppState.initState() => AppState(Colors.red);
}
