import 'package:flutter/cupertino.dart';
import 'package:flutterreduxstatepractice/app_state.dart';
import 'package:flutterreduxstatepractice/selectors.dart';
import 'package:redux/redux.dart';

class MainViewModel {
  final void Function() changeColorOnTheRed;
  final void Function() changeColorOnTheYellow;
  final void Function() changeColorOnTheGreen;
  final Color colors;

  MainViewModel({
    this.colors,
    this.changeColorOnTheGreen,
    this.changeColorOnTheRed,
    this.changeColorOnTheYellow,
  });

  factory MainViewModel.initState(Store<AppState> store) => MainViewModel(
        changeColorOnTheGreen:
            AppStateSelector.getChangeColorOnTheGreenFunction(store),
        changeColorOnTheRed:
            AppStateSelector.getChangeColorOnTheRedFunction(store),
        changeColorOnTheYellow:
            AppStateSelector.getChangeColorOnTheYellowFunction(store),
        colors: AppStateSelector.getCurrentColor(store),
      );
}
