import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterreduxstatepractice/actions.dart';
import 'package:flutterreduxstatepractice/app_state.dart';

AppState appStateReducer(AppState state, dynamic action) {
  print('Test');
  if (action == ChangeColorOnTheRed) {
    return AppState(Colors.red);
  }
  if (action == ChangeColorOnTheGreen) {
    return AppState(Colors.green);
  }
  if (action == ChangeColorOnTheYellow) {
    return AppState(Colors.yellow);
  }
  return state;
}
